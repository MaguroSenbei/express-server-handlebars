

const Gulp              = require('gulp');
const Rename            = require('gulp-rename');
const DefineModule      = require('gulp-define-module');
const CompileHandlebars = require('gulp-precompile-handlebars');

return Gulp.task("html", () => Gulp.src('views/**/*.hbs')
	.pipe(CompileHandlebars())
	.pipe(Rename({ extname: '.js' }))
	.pipe(DefineModule('es6'))
	.pipe(Gulp.dest('dist/templates')));