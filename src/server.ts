import * as Express from 'express';
import * as Handlebars from 'express-handlebars';

const PORT = 4000;

const application = Express();

application.engine("hbs", Handlebars({ 
	defaultLayout: 'base',
	layoutsDir   : 'views/layouts',
	partialsDir  : 'views/partials',
	extname      : ".hbs" })
);
	
application.set('view engine',  'hbs');
application.enable('view cache');

application.get("/", homePage);

application.listen(PORT, () => {
	console.log(`Server listening at port ${PORT}`);
});

function homePage(pRequest: Express.Request, pResponse: Express.Response) {
	console.log("Hello World");
	pResponse.render('base', { layout: false } );
}
